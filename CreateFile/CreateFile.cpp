#undef UNICODE
#include "windows.h"
#include <iostream>
using namespace std;

int main()
{
	HANDLE h = CreateFile ("abc.txt", //  lpFileName,
		                   GENERIC_READ | GENERIC_WRITE, //  dwDesiredAccess,
		                   0, // dwShareMode,
		                   0, // lpSecurityAttributes,
		                   CREATE_ALWAYS, // [in]           DWORD                 dwCreationDisposition,
		                   0, //  dwFlagsAndAttributes,
		                   NULL // hTemplateFile
	                       );

	if (h != INVALID_HANDLE_VALUE)
	{
		const char * buf = "abcd";
		int n = 4;
		DWORD answer;
		bool ok = WriteFile (h, buf, n, &answer, NULL);
		CloseHandle (h);
	}
}
